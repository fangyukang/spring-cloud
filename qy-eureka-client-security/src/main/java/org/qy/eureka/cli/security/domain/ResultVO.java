package org.qy.eureka.cli.security.domain;

import lombok.Data;

import java.io.Serializable;

/**
 * @Description:	统一返回结果类
 * @Author:		方宇康
 * @CreateDate:	2019/1/7
 * @Version:      	1.0.0.1
 * @Company:       联通智网科技有限公司
 */
@Data
@SuppressWarnings({"all"})
public class ResultVO<T> implements Serializable
{
	private static final long serialVersionUID = 5539018865466261955L;

	/** 状态码：1成功，其他为失败 */
	private String code;

	/** 成功为success，其他为失败原因 */
	private String msg;

	/**  数据结果集 */
	private T data;

	public ResultVO()
	{
		
	}
	
	public ResultVO(String code, String msg)
	{
		this.code = code;
		this.msg = msg;
	}

	public ResultVO(String code, String msg, T data)
	{
		this.code = code;
		this.msg = msg;
		this.data = data;
	}

	public static ResultVO build(String code, String msg, Object data)
	{
		return new ResultVO(code, msg, data);
	}

	public static ResultVO build(String code, String msg)
	{
		return new ResultVO(code, msg, null);
	}
}
