package org.qy.eureka.cli.security;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.ApplicationContext;

/**
 * @Description:	des
 * @Author:		方宇康
 * @CreateDate:	2020/4/29 10:56
 * @Version:      	1.0.0.1
 * @Company:       联通智网科技有限公司
 */
@Slf4j
@SpringBootApplication
@EnableDiscoveryClient
public class EurekaClientSecurityApplication
{
    public static void main( String[] args )
    {
        ApplicationContext context = SpringApplication.run(EurekaClientSecurityApplication.class, args);
        String serverPort = context.getEnvironment().getProperty("server.port");
        log.info("EurekaClientSecurity客户端启动成功! Swagger2: http://127.0.0.1:".concat(serverPort));
    }
}
