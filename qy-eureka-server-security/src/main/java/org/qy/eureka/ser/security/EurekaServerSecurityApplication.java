package org.qy.eureka.ser.security;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;
import org.springframework.context.ApplicationContext;

/**
 * @Description:	注册中心启动类
 * @Author:		方宇康
 * @CreateDate:	2020/4/28 16:38
 * @Version:      	1.0.0.1
 * @Company:       联通智网科技有限公司
 */
@Slf4j
@EnableEurekaServer
@SpringBootApplication
public class EurekaServerSecurityApplication
{
    public static void main( String[] args )
    {
        ApplicationContext context = SpringApplication.run(EurekaServerSecurityApplication.class, args);
        String serverPort = context.getEnvironment().getProperty("server.port");
        log.info("Eureka注册中心服务启动成功! Url: http://127.0.0.1:".concat(serverPort));
    }
}
