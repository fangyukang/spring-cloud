package org.qy.rocketmq.cs.service.inter;

import org.qy.rocketmq.cs.domain.entity.User;
import org.qy.rocketmq.cs.rocket.MQFunction;

/**
 * All rights Reserved, Designed By www.cu-sc.com
 *
 * @Title: ISendMessageMQService.java
 * @Package org.qy.rocketmq.cs.service.inter
 * @Description: TODO
 * @Since: JDK 1.8
 * @Author: fangyukang
 * @Email: fangyk@cu-sc.com
 * @Version: v1.0.0
 * @Date: 2020/7/2 13:51
 * @Copyright: 2020 www.cu-sc.com All rights reserved. <br/>
 * 注意：本内容仅限于联通集团内部传阅，禁止外泄以及用于其他的商业目的<br/>
 */
@SuppressWarnings("all")
public interface ISendMessageMQService
{
    void produceMsg(User user, MQFunction fun);
}
