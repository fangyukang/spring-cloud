package org.qy.rocketmq.cs.controller;

import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.qy.rocketmq.cs.domain.vo.ResultVO;
import org.qy.rocketmq.cs.enums.ResultEnum;
import org.qy.rocketmq.cs.service.inter.IProducerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * All rights Reserved, Designed By www.cu-sc.com
 *
 * @Title: ProducerController.java
 * @Package org.qy.rocketmq.cs
 * @Description: TODO
 * @Since: JDK 1.8
 * @Author: fangyukang
 * @Email: fangyk@cu-sc.com
 * @Version: v1.0.0
 * @Date: 2020/6/28 13:00
 * @Copyright: 2020 www.cu-sc.com All rights reserved. <br/>
 * 注意：本内容仅限于联通集团内部传阅，禁止外泄以及用于其他的商业目的<br/>
 */
@Slf4j
@RestController
@RequestMapping("/rocket/producer")
public class ProducerController
{
    @Autowired
    private IProducerService producerService;

    /**
     * @Function: ProducerController.java
     * @Description: 该函数的功能描述
     * @params:
     * @return:
     * @throws: 异常描述
     * @version: v1.0.0
     * @author: fangyukang
     * @date: 2020/7/2 12:00
     * 注意:本内容仅限于联通智网科技有限公司内部传阅,禁止外泄以及用于其他的商业目
     * Modification History:
     * Date              Author          Version         Description
     * 2020/7/2        fangyukang          v1.0.0          修改原因
     */
    @ApiOperation(value = "获取测试MOCK数据", notes = "ProducerController producer")
    @PostMapping("/messageCtrl")
    public ResultVO<?> messageCtrl()
    {
        log.info("ProducerController|messageCtrl|进入获取测试MOCK数据接口服务");
        return ResultVO.build(ResultEnum.SUCCESS.getCode(), ResultEnum.SUCCESS.getMessage(), this.producerService.message());
    }
}
