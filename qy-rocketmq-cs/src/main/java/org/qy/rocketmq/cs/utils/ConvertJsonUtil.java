package org.qy.rocketmq.cs.utils;

import com.alibaba.fastjson.JSONObject;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.LongSerializationPolicy;
import org.springframework.cglib.beans.BeanCopier;

/**
 * @Description:	json object 转换工具类
 * @Author:		方宇康
 * @CreateDate:	2019/1/7
 * @Version:      	1.0.0.1
 * @Company:       联通智网科技有限公司
 */
@SuppressWarnings("ALL")
public class ConvertJsonUtil
{
	/**
	 * VO实例 to json
	 * 
	 * @param obj
	 * @return
	 */
	public static String objectToJson(Object obj)
	{
		return new Gson().toJson(obj);
	}

	/**
	 * json to VO实例
	 *
	 * @param <T>
	 * @param obj
	 * @param json
	 * @return
	 */
	public static <T> Object jsonToObject(String json, Class<T> obj)
	{
		return new Gson().fromJson(json, obj);
	}

	/**
	 * json to VO实例 并且处理对应的科学计数法
	 *
	 * @param <T>
	 * @param obj
	 * @return
	 * @exception   
	 * @date        2019/1/16
	 */
	public static <T> Object jsonToObjectLong(String json, Class<T> obj)
	{
		return new Gson().fromJson(
				new GsonBuilder().setLongSerializationPolicy(LongSerializationPolicy.STRING).create()
						.toJson(JSONObject.parse(json)), obj);
	}

	/**
	 * 实体字段映射
	 * 
	 * @param 		obj1
	 * @param 		obj2
	 * @return
	 * @exception   
	 * @date        2019/1/9 14:59
	 */
	public static void beanCopier(Object obj1, Object obj2)
	{
		BeanCopier copier = BeanCopier.create(obj1.getClass(), obj2.getClass(), false);
		copier.copy(obj1, obj2, null);
	}
}
