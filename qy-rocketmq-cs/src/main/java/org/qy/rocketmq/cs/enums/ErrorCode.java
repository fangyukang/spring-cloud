package org.qy.rocketmq.cs.enums;

import lombok.Getter;
import org.springframework.http.HttpStatus;

/**
 * All rights Reserved, Designed By www.cu-sc.com
 *
 * @Title: ErrorCode.java
 * @Package org.qy.rocketmq.cs.enums
 * @Description: TODO
 * @Since: JDK 1.8
 * @Author: fangyukang
 * @Email: fangyk@cu-sc.com
 * @Version: v1.0.0
 * @Date: 2020/7/1 22:01
 * @Copyright: 2020 www.cu-sc.com All rights reserved. <br/>
 * 注意：本内容仅限于联通集团内部传阅，禁止外泄以及用于其他的商业目的<br/>
 */
public enum ErrorCode
{
    SERVER_ERROR(HttpStatus.INTERNAL_SERVER_ERROR, "v_server_error", "V0082"),
    ;

    @Getter
    private final HttpStatus status;

    @Getter
    private final String code;

    @Getter
    private final String errorCode;

    ErrorCode(HttpStatus status, String code, String errorCode)
    {
        this.status = status;
        this.code = code;
        this.errorCode = errorCode;
    }
}
