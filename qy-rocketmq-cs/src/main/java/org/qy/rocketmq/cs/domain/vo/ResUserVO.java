package org.qy.rocketmq.cs.domain.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.sql.Timestamp;

/**
 * All rights Reserved, Designed By www.cusc.com
 * @title  ResUserVO.java
 * @package org.qy.rocketmq.cs.domain.vo
 * @description  用户信息对外映射
 * @since JDK 1.8
 * @author 方宇康
 * @email fangyk@cu-sc.com
 * @version v1.0.0
 * @date 2020/7/14 16:41
 * @Copyright: 2020/7/14 www.cusc.com Inc. All rights reserved. <br/>
 * 注意：本内容仅限于联通智网科技有限公司内部传阅，禁止外泄以及用于其他的商业目<br/>
 * Modification History:<br/>
 * Date         Author          Version            Description<br/>
 **---------------------------------------------------------*<br/>
 * 2020/7/14      方宇康           v1.0.0               修改原因<br/>
 */
@Data
public class ResUserVO implements Serializable
{
    private static final long serialVersionUID = -5364627087566665057L;

    @ApiModelProperty(value = "主键", dataType = "Long", example = "1")
    private Long id;

    @ApiModelProperty(value = "用户名", dataType = "String")
    private String userName;

    @ApiModelProperty(value = "年龄", dataType = "Integer")
    private Integer userAge;

    @ApiModelProperty(value = "地址", dataType = "String")
    private String address;

    @ApiModelProperty(value = "创建时间", dataType = "Timestamp")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Timestamp createTime;

    @ApiModelProperty(value = "更新时间", dataType = "Timestamp")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Timestamp updateTime;

    @ApiModelProperty(value = "备注信息", dataType = "String")
    private String remarks;
}
