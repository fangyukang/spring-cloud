package org.qy.rocketmq.cs.rocket.listener;

import org.apache.rocketmq.client.producer.TransactionListener;

/**
 * All rights Reserved, Designed By www.cu-sc.com
 *
 * @Title:  ProducerTransactionListener.java.java
 * @Package org.qy.rocketmq.cs.rocket.listener
 * @Description: TODO
 * @Since: JDK 1.8
 * @Author: fangyukang
 * @Email: fangyk@cu-sc.com
 * @Version: v1.0.0
 * @Date: 2020/7/1 22:11
 * @Copyright: 2020 www.cu-sc.com All rights reserved. <br/>
 * 注意：本内容仅限于联通集团内部传阅，禁止外泄以及用于其他的商业目的<br/>
 */
public interface ProducerTransactionListener extends TransactionListener
{

}
