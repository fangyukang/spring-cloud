package org.qy.rocketmq.cs.rocket;

/**
 * All rights Reserved, Designed By www.cu-sc.com
 *
 * @Title:  MQFunction.java.java
 * @Package org.qy.rocketmq.cs.rocket
 * @Description: 事务消息执行处理
 * @Since: JDK 1.8
 * @Author: fangyukang
 * @Email: fangyk@cu-sc.com
 * @Version: v1.0.0
 * @Date: 2020/7/1 22:20
 * @Copyright: 2020 www.cu-sc.com All rights reserved. <br/>
 * 注意：本内容仅限于联通集团内部传阅，禁止外泄以及用于其他的商业目的<br/>
 */
@FunctionalInterface
public interface MQFunction
{
    /**
     * @Description: 该函数的功能描述
     * @Function: MQFunction.java
     * @params:
     * @return:
     * @throws: 异常描述
     * @version: v1.0.0
     * @author: fangyukang
     * @date: 2020/7/1 22:20
     * 注意:本内容仅限于联通智网科技有限公司内部传阅,禁止外泄以及用于其他的商业目
     * Modification History:
     * Date              Author          Version         Description
     * 2020/7/1        fangyukang          v1.0.0          修改原因
     */
    void apply();
}
