package org.qy.rocketmq.cs.utils;

import com.alibaba.fastjson.JSONObject;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import org.apache.commons.lang3.StringUtils;
import org.qy.rocketmq.cs.config.json.LocalDateTimeDeserializer;
import org.qy.rocketmq.cs.config.json.LocalDateTimeSerializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.OutputStream;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

/**
 * All rights Reserved, Designed By www.cu-sc.com
 *
 * @Title:  JsonUtil.java.java
 * @Package org.qy.rocketmq.cs.utils
 * @Description: TODO
 * @Since: JDK 1.8
 * @Author: fangyukang
 * @Email: fangyk@cu-sc.com
 * @Version: v1.0.0
 * @Date: 2020/7/1 22:24
 * @Copyright: 2020 www.cu-sc.com All rights reserved. <br/>
 * 注意：本内容仅限于联通集团内部传阅，禁止外泄以及用于其他的商业目的<br/>
 */
public class JsonUtil
{
    private static final Logger log = LoggerFactory.getLogger(JsonUtil.class);

    private static ObjectMapper objectMapper;

    private JsonUtil(){}

    static
    {
        objectMapper = new ObjectMapper();
        objectMapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
        objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

        SimpleModule simpleModule = new SimpleModule();
        simpleModule.addSerializer(LocalDateTime.class, new LocalDateTimeSerializer());
        simpleModule.addDeserializer(LocalDateTime.class, new LocalDateTimeDeserializer());
        //处理LocalData等序列化问题
        objectMapper.registerModules(new JavaTimeModule(), simpleModule);
    }

    public static String stringify(Object object)
    {
        try {
            return objectMapper.writeValueAsString(object);
        } catch (Exception e) {
            log.error(e.getLocalizedMessage());
        }
        return null;
    }

    public static void stringify(OutputStream out, Object object)
    {
        try {
            objectMapper.writeValue(out, object);
        } catch (Exception e) {
            log.error(e.getLocalizedMessage());
        }
    }

    public static <T> T parse(String json, TypeReference valueTypeRef)
    {
        if (json == null || json.length() == 0) {
            return null;
        }
        try {
            return objectMapper.readValue(json, valueTypeRef);
        } catch (Exception e) {
            log.error(e.getLocalizedMessage());
        }
        return null;
    }

    public static <T> T parse(String json, Class<T> clazz)
    {
        if (json == null || json.length() == 0) {
            return null;
        }
        try {
            return objectMapper.readValue(json, clazz);
        } catch (Exception e) {
            log.error(e.getLocalizedMessage());
        }
        return null;
    }

    public static <T>List parseList(String json, Class<T> clazz)
    {
        if (json == null || json.length() == 0) {
            return new ArrayList<>();
        }
        try {
            Object list = parse(json, Object.class);
            List list1 = new ArrayList<>();
            for(LinkedHashMap map : (List<LinkedHashMap>)list){
                String jsonList = JsonUtil.stringify(map);
                list1.add(JsonUtil.parse(jsonList, clazz));
            }
            return list1;
        } catch (Exception e) {
            log.error(e.getLocalizedMessage());
        }
        return new ArrayList<>();
    }

    public static <T>List parseList(List list, Class<T> clazz)
    {
        try {
            List list1 = new ArrayList<>();
            for(LinkedHashMap map : (List<LinkedHashMap>)list){
                String jsonList = JsonUtil.stringify(map);
                list1.add(JsonUtil.parse(jsonList, clazz));
            }
            return list1;
        } catch (Exception e) {
            log.error(e.getLocalizedMessage());
        }
        return new ArrayList<>();
    }

    public static String fromObject(String srcStr, String key)
    {
        JSONObject jsonObject = JSONObject.parseObject(srcStr);
        String value = (String)jsonObject.get(key);
        if (StringUtils.isEmpty(value)) {
            return null;
        }
        return value;
    }

}
