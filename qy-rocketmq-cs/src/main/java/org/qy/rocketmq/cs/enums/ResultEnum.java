package org.qy.rocketmq.cs.enums;

import lombok.Getter;
import lombok.NoArgsConstructor;

/**
 * All rights Reserved, Designed By www.cusc.com
 * @title  ResultEnum.java
 * @package org.qy.rocketmq.cs.enums
 * @description  公共部分枚举类
 * @since JDK 1.8
 * @author 方宇康
 * @email fangyk@cu-sc.com
 * @version v1.0.0
 * @date 2020/7/14 16:41
 * @Copyright: 2020/7/14 www.cusc.com Inc. All rights reserved. <br/>
 * 注意：本内容仅限于联通智网科技有限公司内部传阅，禁止外泄以及用于其他的商业目<br/>
 * Modification History:<br/>
 * Date         Author          Version            Description<br/>
 **---------------------------------------------------------*<br/>
 * 2020/7/14      方宇康           v1.0.0               修改原因<br/>
 */
@Getter
@NoArgsConstructor
public enum ResultEnum
{
	/** 访问参数异常 */
	ERROR_POST_PARAM("400", "访问参数异常"),
	/** 访问方法异常 */
	ERROR_POST_METHOD("405", "访问方法异常"),
	/** 访问异常 */
	FAILED("500", "访问异常,请稍后重试"),
	/** 访问成功 */
	SUCCESS("200","success"),
	/** 成功 */
	SUCCESS_ONE("0","成功")
    ;

	private String code;
	private String message;

	ResultEnum(String code, String message)
	{
		this.code = code;
		this.message = message;
	}
}