package org.qy.rocketmq.cs.rocket.listener;

import org.apache.rocketmq.client.consumer.listener.ConsumeConcurrentlyContext;
import org.apache.rocketmq.client.consumer.listener.ConsumeConcurrentlyStatus;
import org.apache.rocketmq.client.consumer.listener.MessageListenerConcurrently;
import org.apache.rocketmq.common.message.MessageExt;

import java.util.List;

/**
 * All rights Reserved, Designed By www.cu-sc.com
 *
 * @Title:  ConsumeMessageListener.java.java
 * @Package org.qy.rocketmq.cs.rocket.listener
 * @Description: TODO
 * @Since: JDK 1.8
 * @Author: fangyukang
 * @Email: fangyk@cu-sc.com
 * @Version: v1.0.0
 * @Date: 2020/7/1 22:10
 * @Copyright: 2020 www.cu-sc.com All rights reserved. <br/>
 * 注意：本内容仅限于联通集团内部传阅，禁止外泄以及用于其他的商业目的<br/>
 */
public interface ConsumeMessageListener extends MessageListenerConcurrently
{
    /**
     * @Function: ConsumeMessageListener.java
     * @Description: 该函数的功能描述
     * @params:
     * @return:
     * @throws: 异常描述
     * @version: v1.0.0
     * @author: fangyukang
     * @date: 2020/7/1 22:10
     * 注意:本内容仅限于联通智网科技有限公司内部传阅,禁止外泄以及用于其他的商业目
     * Modification History:
     * Date              Author          Version         Description
     * 2020/7/1        fangyukang          v1.0.0          修改原因
     */
    @Override
    default ConsumeConcurrentlyStatus consumeMessage(List<MessageExt> list, ConsumeConcurrentlyContext consumeConcurrentlyContext)
    {
        return dealBody(list);
    }

    ConsumeConcurrentlyStatus dealBody(List<MessageExt> msg);
}
