package org.qy.rocketmq.cs.config.aop;

import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.qy.rocketmq.cs.utils.JsonUtil;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;

/**
 * All rights Reserved, Designed By www.cu-sc.com
 *
 * @Title:  LogParamAspect.java.java
 * @Package org.qy.rocketmq.cs.config.aop
 * @Description: 打印Controller接口日志 @Order(2) 对AOP执行顺序排序，数值越小越优先
 * @Since: JDK 1.8
 * @Author: fangyukang
 * @Email: fangyk@cu-sc.com
 * @Version: v1.0.0
 * @Date: 2020/7/2 22:52
 * @Copyright: 2020 www.cu-sc.com All rights reserved. <br/>
 * 注意：本内容仅限于联通集团内部传阅，禁止外泄以及用于其他的商业目的<br/>
 */
@Slf4j
@Component
@Order(2)
@Aspect
public class LogParamAspect
{
    private final String printStr = "{} {} | {}";

    /**
     * Before说明 execution(public(方法限定符) *(方法返回值) org.qy.rocketmq.cs.controller..*(包下所有类包括子包).*(..)(所有方法任意参数))
     *
     * @function LogParamAspect.java
     * @param joinPoint
     * @return
     * @throws
     * @version v1.0.0
     * @author 方宇康
     * @date 2020/7/14 16:37<br/>
     *
     * Modification History:<br/>
     * Date         Author          Version            Description<br/>
     **---------------------------------------------------------*<br/>
     * 2020/7/14       方宇康           v1.0.0               修改原因<br/>
     */
    @Before("execution(public * org.qy.rocketmq.cs.controller..*.*(..))")
    public void before(JoinPoint joinPoint)
    {
        HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
        log.info(printStr, request.getMethod(), request.getRequestURI(), JsonUtil.stringify(joinPoint.getArgs()));
    }
}
