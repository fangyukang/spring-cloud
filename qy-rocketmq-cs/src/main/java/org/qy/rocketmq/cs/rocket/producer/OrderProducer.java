package org.qy.rocketmq.cs.rocket.producer;

import com.alibaba.fastjson.JSONObject;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.extern.slf4j.Slf4j;
import org.apache.rocketmq.client.exception.MQClientException;
import org.apache.rocketmq.client.producer.DefaultMQProducer;
import org.apache.rocketmq.client.producer.SendResult;
import org.apache.rocketmq.client.producer.SendStatus;
import org.apache.rocketmq.common.message.Message;
import org.qy.rocketmq.cs.enums.ErrorCode;
import org.qy.rocketmq.cs.exception.BadRequestException;
import org.qy.rocketmq.cs.exception.MQRuntimeException;
import org.qy.rocketmq.cs.utils.JsonUtil;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.nio.charset.StandardCharsets;
import java.util.Objects;

/**
 * All rights Reserved, Designed By www.cu-sc.com
 *
 * @Title:  OrderProducer.java.java
 * @Package org.qy.rocketmq.cs.rocket.producer
 * @Description: 消息生产者
 * @Since: JDK 1.8
 * @Author: fangyukang
 * @Email: fangyk@cu-sc.com
 * @Version: v1.0.0
 * @Date: 2020/7/1 22:34
 * @Copyright: 2020 www.cu-sc.com All rights reserved. <br/>
 * 注意：本内容仅限于联通集团内部传阅，禁止外泄以及用于其他的商业目的<br/>
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Slf4j
@Configuration
@ConfigurationProperties(prefix = "rocketmq.producer")
public class OrderProducer extends DefaultMQProducer
{
    public OrderProducer()
    {
        super();
        log.info("OrderProducer|rocketmq producer 正在创建");
        this.setVipChannelEnabled(false);
        this.setRetryTimesWhenSendAsyncFailed(10);
    }

    /**
     * @Function: OrderProducer.java
     * @Description: 使用事物模式发送
     * @params:
     * @return:
     * @throws: 异常描述
     * @version: v1.0.0
     * @author: fangyukang
     * @date: 2020/7/2 9:34
     * 注意:本内容仅限于联通智网科技有限公司内部传阅,禁止外泄以及用于其他的商业目
     * Modification History:
     * Date              Author          Version         Description
     * 2020/7/2        fangyukang          v1.0.0          修改原因
     */
    public SendResult send(String topic, String tags, Object o)
    {
        return this.send(topic, tags, null, o);
    }

    public SendResult send(String topic, String tags, Integer delayTimeLevel, Object o)
    {
        Message msg = new Message();
        msg.setTopic(topic);
        msg.setTags(tags);
        msg.setBody(JsonUtil.stringify(o).getBytes(StandardCharsets.UTF_8));
        if(Objects.nonNull(delayTimeLevel))
        {
            msg.setDelayTimeLevel(delayTimeLevel);
        }
        return this.send(msg);
    }

    /**
     * @Function: OrderProducer.java
     * @Description: 使用事物模式发送
     * @params:
     * @return:
     * @throws: 异常描述
     * @version: v1.0.0
     * @author: fangyukang
     * @date: 2020/7/2 9:35
     * 注意:本内容仅限于联通智网科技有限公司内部传阅,禁止外泄以及用于其他的商业目
     * Modification History:
     * Date              Author          Version         Description
     * 2020/7/2        fangyukang          v1.0.0          修改原因
     */
    @Override
    public SendResult send(Message msg)
    {
        try {
            // sendMessageInTransaction 事物消息不支持延时消息
            SendResult result = super.send(msg);
            log.info("OrderProducer|send|使用事物模式发送消息|返回结果：={}", JSONObject.toJSONString(result));
            if(result.getSendStatus() != SendStatus.SEND_OK)
            {
                throw new MQRuntimeException(String.format("mq send error sendStatus:%s", result.getSendStatus().name()));
            }
            return result;
        }
        catch (Exception e)
        {
            throw new BadRequestException(ErrorCode.SERVER_ERROR);
        }
    }

    @PostConstruct
    @Override
    public void start()
    {
        try {
            super.start();
            log.info("OrderProducer|start|rocketmq producer server 启动成功");
        } catch (MQClientException e) {
            log.error("OrderProducer|start|rocketmq producer server 启动失败", e);
        }
    }

    @PreDestroy
    @Override
    public void shutdown()
    {
        super.shutdown();
        log.info("OrderProducer|shutdown|rocketmq producer server 关闭成功");
    }
}
