package org.qy.rocketmq.cs.utils;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;

/**
 * All rights Reserved, Designed By www.cu-sc.com
 *
 * @Title:  TimeUtils.java.java
 * @Package org.qy.rocketmq.cs.utils
 * @Description: TODO
 * @Since: JDK 1.8
 * @Author: fangyukang
 * @Email: fangyk@cu-sc.com
 * @Version: v1.0.0
 * @Date: 2020/7/1 22:25
 * @Copyright: 2020 www.cu-sc.com All rights reserved. <br/>
 * 注意：本内容仅限于联通集团内部传阅，禁止外泄以及用于其他的商业目的<br/>
 */
public class TimeUtils
{
    private static ZoneOffset getZoneOffset(LocalDateTime temporal)
    {
        return ZoneOffset.from(getZoneId() .getRules().getOffset(temporal));
    }

    private static ZoneId getZoneId()
    {
        return ZoneId.systemDefault();
    }

    /**
     * @Function: TimeUtils.java
     * @Description: 转换成秒
     * @params:
     * @return:
     * @throws: 异常描述
     * @version: v1.0.0
     * @author: fangyukang
     * @date: 2020/7/1 22:24
     * 注意:本内容仅限于联通智网科技有限公司内部传阅,禁止外泄以及用于其他的商业目
     * Modification History:
     * Date              Author          Version         Description
     * 2020/7/1        fangyukang          v1.0.0          修改原因
     */
    public static Long toSecond(LocalDateTime dateTime)
    {
        return dateTime.toEpochSecond(getZoneOffset(dateTime));
    }

    /**
     * @Function: TimeUtils.java
     * @Description: 转换成时间戳毫秒
     * @params:
     * @return:
     * @throws: 异常描述
     * @version: v1.0.0
     * @author: fangyukang
     * @date: 2020/7/1 22:25
     * 注意:本内容仅限于联通智网科技有限公司内部传阅,禁止外泄以及用于其他的商业目
     * Modification History:
     * Date              Author          Version         Description
     * 2020/7/1        fangyukang          v1.0.0          修改原因
     */
    public static Long toTimestamp(LocalDateTime dateTime)
    {
        return Long.parseLong(dateTime.toEpochSecond(getZoneOffset(dateTime)) + "000");
    }

    /**
     * @Function: TimeUtils.java
     * @Description: 毫秒数转时间戳
     * @params:
     * @return:
     * @throws: 异常描述
     * @version: v1.0.0
     * @author: fangyukang
     * @date: 2020/7/1 22:25
     * 注意:本内容仅限于联通智网科技有限公司内部传阅,禁止外泄以及用于其他的商业目
     * Modification History:
     * Date              Author          Version         Description
     * 2020/7/1        fangyukang          v1.0.0          修改原因
     */
    public static LocalDateTime toLocalDateTime(long timestamp)
    {
        Instant instant = Instant.ofEpochMilli(timestamp);
        return LocalDateTime.ofInstant(instant, getZoneId());
    }
}
