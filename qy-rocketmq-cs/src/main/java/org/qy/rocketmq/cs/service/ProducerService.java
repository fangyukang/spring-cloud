package org.qy.rocketmq.cs.service;
import java.sql.Timestamp;

import lombok.extern.slf4j.Slf4j;
import org.qy.rocketmq.cs.domain.entity.User;
import org.qy.rocketmq.cs.rocket.MQFunction;
import org.qy.rocketmq.cs.service.inter.IProducerService;
import org.qy.rocketmq.cs.service.inter.ISendMessageMQService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * All rights Reserved, Designed By www.cu-sc.com
 *
 * @Title: ProducerService.java
 * @Package org.qy.rocketmq.cs.service
 * @Description: TODO
 * @Since: JDK 1.8
 * @Author: fangyukang
 * @Email: fangyk@cu-sc.com
 * @Version: v1.0.0
 * @Date: 2020/7/2 11:58
 * @Copyright: 2020 www.cu-sc.com All rights reserved. <br/>
 * 注意：本内容仅限于联通集团内部传阅，禁止外泄以及用于其他的商业目的<br/>
 */
@Slf4j
@Service
public class ProducerService implements IProducerService
{
    @Autowired
    ISendMessageMQService sendMessageMQService;

    @Autowired
    IProducerService producerService;

    @Autowired
    UserService userService;

    @Override
    public Object message()
    {
        MQFunction fun = () -> this.producerService.save();
        User user = new User();
        user.setId(0L);
        user.setUserName("张三");
        user.setUserAge(0);
        user.setAddress("十字路口");
        user.setCreateTime(new Timestamp(new java.util.Date().getTime()));
        user.setUpdateTime(new Timestamp(new java.util.Date().getTime()));
        user.setRemarks("天王盖地虎");
        this.sendMessageMQService.produceMsg(user, fun);
        return true;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void save()
    {
        log.info("ProducerService|save|加上事务控制，执行消息保存操作");
        User user = new User();
        user.setId(3L);
        user.setRemarks("加上事务控制");
        this.userService.updateById(user);
    }
}
