package org.qy.rocketmq.cs.config.json;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import org.qy.rocketmq.cs.utils.TimeUtils;

import java.io.IOException;
import java.time.LocalDateTime;

/**
 * All rights Reserved, Designed By www.cu-sc.com
 *
 * @Title:  LocalDateTimeSerializer.java.java
 * @Package org.qy.rocketmq.cs.config.json
 * @Description: LocalDateTime 序列化成Long时间戳
 * @Since: JDK 1.8
 * @Author: fangyukang
 * @Email: fangyk@cu-sc.com
 * @Version: v1.0.0
 * @Date: 2020/7/1 22:16
 * @Copyright: 2020 www.cu-sc.com All rights reserved. <br/>
 * 注意：本内容仅限于联通集团内部传阅，禁止外泄以及用于其他的商业目的<br/>
 */
public class LocalDateTimeSerializer extends JsonSerializer<LocalDateTime>
{
    @Override
    public void serialize(LocalDateTime value, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException
    {
        jsonGenerator.writeNumber(TimeUtils.toTimestamp(value));
    }
}
