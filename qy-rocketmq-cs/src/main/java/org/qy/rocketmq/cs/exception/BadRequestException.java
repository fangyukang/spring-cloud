package org.qy.rocketmq.cs.exception;

import org.qy.rocketmq.cs.enums.ErrorCode;

import java.util.Objects;

/**
 * All rights Reserved, Designed By www.cu-sc.com
 *
 * @Title:  BadRequestException.java.java
 * @Package org.qy.rocketmq.cs.exception
 * @Description: TODO
 * @Since: JDK 1.8
 * @Author: fangyukang
 * @Email: fangyk@cu-sc.com
 * @Version: v1.0.0
 * @Date: 2020/7/1 22:03
 * @Copyright: 2020 www.cu-sc.com All rights reserved. <br/>
 * 注意：本内容仅限于联通集团内部传阅，禁止外泄以及用于其他的商业目的<br/>
 */
public class BadRequestException extends RuntimeException
{
    private final ErrorCode err;

    /** 对应I18N占位符 */
    private String[] args;

    public BadRequestException(ErrorCode err)
    {
        super();
        this.err = err;
    }

    public BadRequestException(ErrorCode err, String... args)
    {
        this(err);
        this.args = args;
    }


    public ErrorCode getErr()
    {
        return err;
    }

    public String[] getArgs()
    {
        return args;
    }

    /**
     * @Function: BadRequestException.java
     * @Description: 设置I18n 占位符值
     * @params:
     * @return:
     * @throws: 异常描述
     * @version: v1.0.0
     * @author: fangyukang
     * @date: 2020/7/1 22:17
     * 注意:本内容仅限于联通智网科技有限公司内部传阅,禁止外泄以及用于其他的商业目
     * Modification History:
     * Date              Author          Version         Description
     * 2020/7/1        fangyukang          v1.0.0          修改原因
     */
    public BadRequestException args(String... args)
    {
        this.args = args;
        return this;
    }

    @Override
    public String getMessage()
    {
        if(Objects.isNull(args))
        {
            return err.getCode();
        }
        return String.format("%s : %s", err.getCode(), String.join(",", args));
    }
}
