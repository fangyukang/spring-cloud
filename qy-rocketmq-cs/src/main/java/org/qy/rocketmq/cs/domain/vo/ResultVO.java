package org.qy.rocketmq.cs.domain.vo;

import io.swagger.annotations.ApiModel;
import lombok.Data;

import java.io.Serializable;

/**
 * All rights Reserved, Designed By www.cusc.com
 * @title  ResultVO.java
 * @package org.qy.rocketmq.cs.domain.vo
 * @description  统一返回结果类
 * @since JDK 1.8
 * @author 方宇康
 * @email fangyk@cu-sc.com
 * @version v1.0.0
 * @date 2020/7/14 16:40
 * @Copyright: 2020/7/14 www.cusc.com Inc. All rights reserved. <br/>
 * 注意：本内容仅限于联通智网科技有限公司内部传阅，禁止外泄以及用于其他的商业目<br/>
 * Modification History:<br/>
 * Date         Author          Version            Description<br/>
 **---------------------------------------------------------*<br/>
 * 2020/7/14      方宇康           v1.0.0               修改原因<br/>
 */
@Data
@ApiModel(description = "统一返回结果类")
@SuppressWarnings({"all"})
public class ResultVO<T> implements Serializable
{
	private static final long serialVersionUID = 5539018865466261955L;

	/** 状态码：1成功，其他为失败 */
	private String code;

	/** 成功为success，其他为失败原因 */
	private String msg;

	/**  数据结果集 */
	private T data;

	public ResultVO()
	{
		
	}
	
	public ResultVO(String code, String msg)
	{
		this.code = code;
		this.msg = msg;
	}

	public ResultVO(String code, String msg, T data)
	{
		this.code = code;
		this.msg = msg;
		this.data = data;
	}

	public static ResultVO build(String code, String msg, Object data)
	{
		return new ResultVO(code, msg, data);
	}

	public static ResultVO build(String code, String msg)
	{
		return new ResultVO(code, msg, null);
	}
}
