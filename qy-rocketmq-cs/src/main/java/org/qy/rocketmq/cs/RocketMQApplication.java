package org.qy.rocketmq.cs;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.ApplicationContext;

/**
 * All rights Reserved, Designed By www.cu-sc.com
 *
 * @Title: org.qy.rocketmq.cs.RocketMQApplication.java
 * @Package PACKAGE_NAME
 * @Description: TODO
 * @Since: JDK 1.8
 * @Author: fangyukang
 * @Email: fangyk@cu-sc.com
 * @Version: v1.0.0
 * @Date: 2020/6/28 12:52
 * @Copyright: 2020 www.cu-sc.com All rights reserved. <br/>
 * 注意：本内容仅限于联通集团内部传阅，禁止外泄以及用于其他的商业目的<br/>
 */
@Slf4j
@SpringBootApplication
@EnableConfigurationProperties
public class RocketMQApplication
{
    public static void main( String[] args )
    {
        ApplicationContext context = SpringApplication.run(RocketMQApplication.class, args);
        String serverPort = context.getEnvironment().getProperty("server.port");
        log.info("后台管理服务客户端启动成功! Swagger2: http://127.0.0.1:".concat(serverPort).concat("/swagger-ui.html"));
    }
}
