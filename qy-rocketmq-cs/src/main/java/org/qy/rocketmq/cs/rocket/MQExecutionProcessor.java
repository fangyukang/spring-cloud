package org.qy.rocketmq.cs.rocket;

import lombok.Data;

/**
 * All rights Reserved, Designed By www.cu-sc.com
 *
 * @Title:  MQExecutionProcessor.java.java
 * @Package org.qy.rocketmq.cs.rocket
 * @Description: 执行结果
 * @Since: JDK 1.8
 * @Author: fangyukang
 * @Email: fangyk@cu-sc.com
 * @Version: v1.0.0
 * @Date: 2020/7/2 9:39
 * @Copyright: 2020 www.cu-sc.com All rights reserved. <br/>
 * 注意：本内容仅限于联通集团内部传阅，禁止外泄以及用于其他的商业目的<br/>
 */
@SuppressWarnings("all")
@Data
public class MQExecutionProcessor
{
    private MQFunction fun;

    /** 执行结果 成功：true  失败：false */
    private Boolean result = true;

    private RuntimeException e;

    public MQExecutionProcessor(MQFunction fun)
    {
        this.fun = fun;
    }
}
