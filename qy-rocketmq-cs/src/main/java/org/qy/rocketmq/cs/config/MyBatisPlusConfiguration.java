package org.qy.rocketmq.cs.config;

import com.baomidou.mybatisplus.extension.plugins.OptimisticLockerInterceptor;
import com.baomidou.mybatisplus.extension.plugins.PaginationInterceptor;
import com.baomidou.mybatisplus.extension.plugins.PerformanceInterceptor;
import org.springframework.context.annotation.*;
import org.springframework.context.annotation.Bean;

/**
 * All rights Reserved, Designed By www.cusc.com
 * @title  MyBatisPlusConfiguration.java
 * @package org.qy.rocketmq.cs.config
 * @description  mybatis plus 插件配置
 * @since JDK 1.8
 * @author 方宇康
 * @email fangyk@cu-sc.com
 * @version v1.0.0
 * @date 2020/7/14 16:36
 * @Copyright: 2020/7/14 www.cusc.com Inc. All rights reserved. <br/>
 * 注意：本内容仅限于联通智网科技有限公司内部传阅，禁止外泄以及用于其他的商业目<br/>
 * Modification History:<br/>
 * Date         Author          Version            Description<br/>
 **---------------------------------------------------------*<br/>
 * 2020/7/14      方宇康           v1.0.0               修改原因<br/>
 */
@Configuration
public class MyBatisPlusConfiguration
{
    /**
     * 乐观锁插件
     * 乐观锁实现方式：
     * <p>取出记录时，获取当前version
     * 更新时，带上这个version
     * 执行更新时， set version = newVersion where version = oldVersion
     * 如果version不对，就更新失败</p>
     * <p>
     *     注解实体字段 @Version 必须要!
     * </p>
     * @return
     */
    @Bean
    public OptimisticLockerInterceptor optimisticLockerInterceptor()
    {
        return new OptimisticLockerInterceptor();
    }

    /**
     * 分页插件
     */
    @Bean
    public PaginationInterceptor paginationInterceptor()
    {
        return new PaginationInterceptor();
    }

    /**
     * SQL执行效率插件
     */
    @Bean
    @Profile({"dev","test","gr"})
    public PerformanceInterceptor performanceInterceptor()
    {
        return new PerformanceInterceptor();
    }
}
