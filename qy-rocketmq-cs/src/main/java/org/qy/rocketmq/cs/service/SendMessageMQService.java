package org.qy.rocketmq.cs.service;

import lombok.extern.slf4j.Slf4j;
import org.qy.rocketmq.cs.domain.entity.User;
import org.qy.rocketmq.cs.rocket.MQFunction;
import org.qy.rocketmq.cs.rocket.producer.ProducerTransaction;
import org.qy.rocketmq.cs.service.inter.ISendMessageMQService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * All rights Reserved, Designed By www.cu-sc.com
 *
 * @Title: SendMessageMQService.java
 * @Package org.qy.rocketmq.cs.service
 * @Description: TODO
 * @Since: JDK 1.8
 * @Author: fangyukang
 * @Email: fangyk@cu-sc.com
 * @Version: v1.0.0
 * @Date: 2020/7/2 13:52
 * @Copyright: 2020 www.cu-sc.com All rights reserved. <br/>
 * 注意：本内容仅限于联通集团内部传阅，禁止外泄以及用于其他的商业目的<br/>
 */
@Slf4j
@Service
@SuppressWarnings("all")
public class SendMessageMQService implements ISendMessageMQService
{
    @Autowired
    private ProducerTransaction producerTransaction;

    @Override
    public void produceMsg(User user, MQFunction fun)
    {
        this.producerTransaction.send("MALL", "ORDER_PLACE_SUCCESS", user, fun);
    }
}
