package org.qy.rocketmq.cs.config.json;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import org.qy.rocketmq.cs.utils.TimeUtils;

import java.io.IOException;
import java.time.LocalDateTime;

/**
 * All rights Reserved, Designed By www.cu-sc.com
 *
 * @Title:  LocalDateTimeDeserializer.java.java
 * @Package org.qy.rocketmq.cs.config.json
 * @Description: 时间戳（long）反序列化成LocalDateTime
 * @Since: JDK 1.8
 * @Author: fangyukang
 * @Email: fangyk@cu-sc.com
 * @Version: v1.0.0
 * @Date: 2020/7/1 22:05
 * @Copyright: 2020 www.cu-sc.com All rights reserved. <br/>
 * 注意：本内容仅限于联通集团内部传阅，禁止外泄以及用于其他的商业目的<br/>
 */
public class LocalDateTimeDeserializer extends StdDeserializer<LocalDateTime>
{
    public LocalDateTimeDeserializer()
    {
        super(Object.class);
    }

    @Override
    public LocalDateTime deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException
    {
        return TimeUtils.toLocalDateTime(jsonParser.getLongValue());
    }
}
