package org.qy.rocketmq.cs.service;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.qy.rocketmq.cs.domain.entity.User;
import org.qy.rocketmq.cs.domain.vo.ResUserVO;
import org.qy.rocketmq.cs.domain.vo.ResultVO;
import org.qy.rocketmq.cs.enums.ResultEnum;
import org.qy.rocketmq.cs.utils.ConvertJsonUtil;
import org.springframework.stereotype.Service;

/**
 * @Description:	业务层封装
 * @Author:		方宇康
 * @CreateDate:	2020/4/29 14:33
 * @Version:      	1.0.0.1
 * @Company:       联通智网科技有限公司
 */
@Slf4j
@Service
public class UserService extends ServiceImpl<BaseMapper<User>, User>
{

    /**
     * 修改用户信息
     *
     * @return
     * @exception
     * @date        2020/4/29 14:30
     */
    public ResultVO<?> editUser(ResUserVO user)
    {
        User userInner = new User();
        ConvertJsonUtil.beanCopier(user, userInner);
        if (this.updateById(userInner))
        {
            return new ResultVO(ResultEnum.SUCCESS.getCode(), ResultEnum.SUCCESS.getMessage());
        }
        return new ResultVO(ResultEnum.FAILED.getCode(), ResultEnum.FAILED.getMessage());
    }
}
