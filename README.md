# spring-cloud

#### 介绍
微服务架构研究，研究模块：Eureka、Config、Admin、Gateway、Feign、Hystrix、Https、Weedfs等等。目的是为了理清微服务的整个架构，作为初始框架使用，不涉及任何业务模块。

#### 软件架构
前期规划
eureka
config
api
gateway
admin
mongodb
weedfs
zipkin

#### 端口占用情况
qy-eureka-server 8761

qy-eureka-server-security 8762

qy-eureka-client 8081

qy-eureka-client-security 8087

qy-swagger-client 8082

qy-mybatisplus-client 8083

qy-config-server 8084

qy-gateway-server 8085

qy-admin-server 8086

qy-rocketmq-cs 
