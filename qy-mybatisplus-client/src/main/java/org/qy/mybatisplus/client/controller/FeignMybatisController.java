package org.qy.mybatisplus.client.controller;

import lombok.extern.slf4j.Slf4j;
import org.qy.mybatisplus.client.domain.vo.ResultVO;
import org.qy.mybatisplus.client.service.EurekaClientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Description:	des
 * @Author:		方宇康
 * @CreateDate:	2020/5/7 10:13
 * @Version:      	1.0.0.1
 * @Company:       联通智网科技有限公司
 */
@Slf4j
@RestController
@RequestMapping(value = "/mybatis-client/feign")
public class FeignMybatisController
{
    private final EurekaClientService eurekaClientService;

    @Autowired
    public FeignMybatisController(EurekaClientService eurekaClientService)
    {
        this.eurekaClientService = eurekaClientService;
    }

    /**
     * feign调用信息获取
     *
     * @return
     * @exception
     * @date        2020/5/7 10:17
     */
    @GetMapping(value = "/info")
    public ResultVO<?> feignInfo()
    {
        log.info("FeignMybatisController|feignInfo|通过FEIGN调用方式获取数据");
        return this.eurekaClientService.feignInfo();
    }
}
