package org.qy.mybatisplus.client.service;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.qy.mybatisplus.client.domain.entity.User;
import org.qy.mybatisplus.client.domain.vo.ResUserVO;
import org.qy.mybatisplus.client.domain.vo.ResultVO;
import org.qy.mybatisplus.client.enums.ResultEnum;
import org.qy.mybatisplus.client.utils.ConvertJsonUtil;
import org.springframework.stereotype.Service;

/**
 * @Description:	业务层封装
 * @Author:		方宇康
 * @CreateDate:	2020/4/29 14:33
 * @Version:      	1.0.0.1
 * @Company:       联通智网科技有限公司
 */
@Slf4j
@Service
public class UserService extends ServiceImpl<BaseMapper<User>, User>
{
    /**
     * 获取用户信息
     *
     * @return
     * @exception
     * @date        2020/4/29 14:35
     */
    public ResultVO<?> addUser(ResUserVO user)
    {
        User userInner = new User();
        ConvertJsonUtil.beanCopier(user, userInner);
        if (this.save(userInner))
        {
            return new ResultVO(ResultEnum.SUCCESS);
        }
        return new ResultVO(ResultEnum.FAILED);
    }

    /**
     * 删除用户信息
     *
     * @return
     * @exception
     * @date        2020/4/29 14:30
     */
    public ResultVO<?> removeUser(Long id)
    {
        if (this.removeById(id))
        {
            return new ResultVO(ResultEnum.SUCCESS);
        }
        return new ResultVO(ResultEnum.FAILED);
    }

    /**
     * 修改用户信息
     *
     * @return
     * @exception
     * @date        2020/4/29 14:30
     */
    public ResultVO<?> editUser(ResUserVO user)
    {
        User userInner = new User();
        ConvertJsonUtil.beanCopier(user, userInner);
        if (this.updateById(userInner))
        {
            return new ResultVO(ResultEnum.SUCCESS);
        }
        return new ResultVO(ResultEnum.FAILED);
    }

    /**
     * 查询用户信息
     *
     * @return
     * @exception
     * @date        2020/4/29 14:30
     */
    public ResultVO<?> inquireUser(Long id)
    {
        return new ResultVO(ResultEnum.SUCCESS, this.getById(id));
    }
}
