package org.qy.mybatisplus.client.service;

import lombok.extern.slf4j.Slf4j;
import org.qy.mybatisplus.client.domain.vo.ResultVO;
import org.qy.mybatisplus.client.enums.ResultEnum;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.stereotype.Service;

/**
 * @Description: 刷新类
 * @Author:		方宇康
 * @CreateDate:	2020/4/29 14:33
 * @Version:      	1.0.0.1
 * @Company:       联通智网科技有限公司
 */
@Slf4j
@Service
@RefreshScope
public class RefreshService
{
    @Value("${testname}")
    public String userName;

    /**
     * 刷新远程配置
     *
     * @return
     * @exception
     * @date        2020/4/29 14:30
     */
    public ResultVO<?> refresh(Long id)
    {
        log.info("RefreshService|refresh|数据字段值刷新|请求入参：={}，={}", id, this.userName);
        return ResultVO.build(ResultEnum.SUCCESS.getCode(), ResultEnum.SUCCESS.getMessage(), this.userName);
    }
}
