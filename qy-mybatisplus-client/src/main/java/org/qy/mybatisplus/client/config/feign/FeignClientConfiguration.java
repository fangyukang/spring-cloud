package org.qy.mybatisplus.client.config.feign;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * All rights Reserved, Designed By www.cusc.com
 * @title  FeignClientConfiguration.java
 * @package com.faw.app.charge.config.fegin
 * @description  日志级别设置
 * @since JDK 1.8
 * @author 方宇康
 * @email fangyk@cu-sc.com
 * @version v1.0.0
 * @date 2019/12/10 17:39
 * @Copyright: 2019/12/10 www.cusc.com Inc. All rights reserved. <br/>
 * 注意：本内容仅限于联通智网科技有限公司内部传阅，禁止外泄以及用于其他的商业目<br/>
 * Modification History:<br/>
 * Date         Author          Version            Description<br/>
 **---------------------------------------------------------*<br/>
 * 2019/12/10      方宇康           v1.0.0               修改原因<br/>
 */
@Configuration
public class FeignClientConfiguration
{
    /**
     *   Logger.Level 的具体级别如下：
     *          NONE：不记录任何信息
     *          BASIC：仅记录请求方法、URL以及响应状态码和执行时间
     *          HEADERS：除了记录 BASIC级别的信息外，还会记录请求和响应的头信息
     *          FULL：记录所有请求与响应的明细，包括头信息、请求体、元数据
     */
    @Bean
    feign.Logger.Level feignLoggerLevel()
    {
        return feign.Logger.Level.FULL;
    }
}
