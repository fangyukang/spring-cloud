package org.qy.mybatisplus.client.service;

import lombok.extern.slf4j.Slf4j;
import org.qy.mybatisplus.client.domain.vo.ResultVO;
import org.qy.mybatisplus.client.service.feign.EurekaClientFeign;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @Description:	des
 * @Author:		方宇康
 * @CreateDate:	2020/5/7 10:32
 * @Version:      	1.0.0.1
 * @Company:       联通智网科技有限公司
 */
@Slf4j
@Service
public class EurekaClientService
{
    private final EurekaClientFeign eurekaClientFeign;

    @Autowired
    public EurekaClientService(EurekaClientFeign eurekaClientFeign) {
        this.eurekaClientFeign = eurekaClientFeign;
    }

    /**
     * 信息获取
     *
     * @return
     * @exception
     * @date        2020/5/7 10:34
     */
    public ResultVO<?> feignInfo()
    {
        log.info("EurekaClientService|feignInfo|信息获取");
        ResultVO<?> resultVO = this.eurekaClientFeign.feignInfo();
        log.info("EurekaClientService|feignInfo|信息获取|请求出参：={}", resultVO);
        return resultVO;
    }
}
