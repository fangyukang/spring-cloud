package org.qy.mybatisplus.client.domain.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.sql.Timestamp;

/**
 * @Description:	des
 * @Author:		方宇康
 * @CreateDate:	2020/4/29 14:36
 * @Version:      	1.0.0.1
 * @Company:       联通智网科技有限公司
 */
@Data
@TableName("user")
public class User implements Serializable
{
    private static final long serialVersionUID = 1979352523248954320L;

    @ApiModelProperty(value = "主键", dataType = "Long", example = "1")
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    @ApiModelProperty(value = "用户名", dataType = "String")
    @TableField("user_name")
    private String userName;

    @ApiModelProperty(value = "年龄", dataType = "Integer")
    @TableField("user_age")
    private Integer userAge;

    @ApiModelProperty(value = "地址", dataType = "String")
    @TableField("address")
    private String address;

    @ApiModelProperty(value = "创建时间", dataType = "Timestamp")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @TableField("create_time")
    private Timestamp createTime;

    @ApiModelProperty(value = "更新时间", dataType = "Timestamp")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @TableField("update_time")
    private Timestamp updateTime;

    @ApiModelProperty(value = "备注信息", dataType = "String")
    @TableField("remarks")
    private String remarks;
}
