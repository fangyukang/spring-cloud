package org.qy.mybatisplus.client.service.feign;

import org.qy.mybatisplus.client.config.feign.FeignClientConfiguration;
import org.qy.mybatisplus.client.domain.vo.ResultVO;
import org.qy.mybatisplus.client.service.fallback.EurekaClientFallback;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * @Description:	feign服务调用
 * @Author:		方宇康
 * @CreateDate:	2020/5/7 10:22
 * @Version:      	1.0.0.1
 * @Company:       联通智网科技有限公司
 */
@FeignClient(value="qy-eureka-client", fallbackFactory = EurekaClientFallback.class, configuration = {FeignClientConfiguration.class})
public interface EurekaClientFeign
{
    /**
     * feign调用信息获取
     *
     * @return
     * @exception
     * @date        2020/5/7 10:17
     */
    @GetMapping(value = "/eureka-client/feign/info")
    ResultVO<?> feignInfo();
}
