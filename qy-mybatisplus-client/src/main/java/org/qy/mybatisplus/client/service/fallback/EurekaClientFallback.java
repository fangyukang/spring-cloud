package org.qy.mybatisplus.client.service.fallback;

import feign.hystrix.FallbackFactory;
import org.qy.mybatisplus.client.service.feign.EurekaClientFeign;

/**
 * @Description:	des
 * @Author:		方宇康
 * @CreateDate:	2020/5/7 10:28
 * @Version:      	1.0.0.1
 * @Company:       联通智网科技有限公司
 */
public class EurekaClientFallback implements FallbackFactory<EurekaClientFeign>
{
    @Override
    public EurekaClientFeign create(Throwable cause)
    {
        return null;
    }
}
