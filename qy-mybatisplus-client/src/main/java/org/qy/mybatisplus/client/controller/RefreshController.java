package org.qy.mybatisplus.client.controller;

import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.qy.mybatisplus.client.domain.vo.ResultVO;
import org.qy.mybatisplus.client.service.RefreshService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.*;

/**
 * @Description:	配置动态刷新机制API
 * @Author:		方宇康
 * @CreateDate:	2020/5/4 11:49
 * @Version:      	1.0.0.1
 * @Company:       联通智网科技有限公司
 */
@Slf4j
@RefreshScope
@RestController
@RequestMapping("config")
public class RefreshController
{
    @Value("${testname}")
    public String userName;

    private final RefreshService refreshService;

    @Autowired
    public RefreshController(RefreshService refreshService)
    {
        this.refreshService = refreshService;
    }

    /**
     * 刷新远程配置
     *
     * @return
     * @exception
     * @date        2020/4/29 14:30
     */
    @ApiOperation(value = "刷新远程配置", notes = "RefreshController refresh")
    @PutMapping("/bus")
    public ResultVO<?> refresh(@RequestParam Long id)
    {
        log.info("RefreshController|refresh|数据字段值刷新|请求入参：={}", this.userName);
        return this.refreshService.refresh(id);
    }
}
