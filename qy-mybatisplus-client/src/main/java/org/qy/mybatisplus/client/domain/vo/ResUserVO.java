package org.qy.mybatisplus.client.domain.vo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.sql.Timestamp;

/**
 * @Description:	用户信息对外映射
 * @Author:		方宇康
 * @CreateDate:	2020/4/29 15:42
 * @Version:      	1.0.0.1
 * @Company:       联通智网科技有限公司
 */
@Data
public class ResUserVO implements Serializable
{
    private static final long serialVersionUID = -5364627087566665057L;

    @ApiModelProperty(value = "主键", dataType = "Long", example = "1")
    private Long id;

    @ApiModelProperty(value = "用户名", dataType = "String")
    private String userName;

    @ApiModelProperty(value = "年龄", dataType = "Integer")
    private Integer userAge;

    @ApiModelProperty(value = "地址", dataType = "String")
    private String address;

    @ApiModelProperty(value = "创建时间", dataType = "Timestamp")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Timestamp createTime;

    @ApiModelProperty(value = "更新时间", dataType = "Timestamp")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Timestamp updateTime;

    @ApiModelProperty(value = "备注信息", dataType = "String")
    private String remarks;
}
