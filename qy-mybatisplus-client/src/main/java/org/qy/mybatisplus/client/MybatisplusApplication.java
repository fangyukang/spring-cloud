package org.qy.mybatisplus.client;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.scheduling.annotation.EnableAsync;

/**
 * @Description:	客户端 mybatisplus集成
 * @Author:		方宇康
 * @CreateDate:	2020/4/29 11:43
 * @Version:      	1.0.0.1
 * @Company:       联通智网科技有限公司
 */
@Slf4j
@EnableAsync
@RefreshScope
@EnableFeignClients
@EnableDiscoveryClient
@SpringBootApplication(scanBasePackages = {"org.qy.mybatisplus.client"})
public class MybatisplusApplication
{
    public static void main( String[] args )
    {
        ApplicationContext context = SpringApplication.run(MybatisplusApplication.class, args);
        String serverPort = context.getEnvironment().getProperty("server.port");
        log.info("后台管理服务客户端启动成功! Swagger2: http://127.0.0.1:".concat(serverPort).concat("/swagger-ui.html"));
    }

    @Bean
    public static PropertySourcesPlaceholderConfigurer placeholderConfigurer()
    {
        PropertySourcesPlaceholderConfigurer placeholder = new PropertySourcesPlaceholderConfigurer();
        placeholder.setIgnoreUnresolvablePlaceholders(true);
        return placeholder;
    }
}
