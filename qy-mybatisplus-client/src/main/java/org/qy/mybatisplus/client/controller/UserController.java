package org.qy.mybatisplus.client.controller;

import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.qy.mybatisplus.client.domain.vo.ResUserVO;
import org.qy.mybatisplus.client.domain.vo.ResultVO;
import org.qy.mybatisplus.client.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @Description:	des
 * @Author:		方宇康
 * @CreateDate:	2020/4/29 11:49
 * @Version:      	1.0.0.1
 * @Company:       联通智网科技有限公司
 */
@Slf4j
@RestController
@RequestMapping("user")
public class UserController
{
    private final UserService userService;

    @Autowired
    public UserController(UserService userService)
    {
        this.userService = userService;
    }

    /**
     * 添加用户信息
     *
     * @return
     * @exception
     * @date        2020/4/29 14:30
     */
    @ApiOperation(value = "添加用户信息", notes = "UserController user add")
    @PutMapping("/add")
    public ResultVO<?> addUser(@RequestBody ResUserVO user)
    {
        log.info("UserController|addUser|添加用户信息|请求入参：={}", user);
        return this.userService.addUser(user);
    }

    /**
     * 删除用户信息
     *
     * @return
     * @exception
     * @date        2020/4/29 14:30
     */
    @ApiOperation(value = "删除用户信息", notes = "UserController user add")
    @DeleteMapping("/remove")
    public ResultVO<?> removeUser(@RequestParam Long id)
    {
        log.info("UserController|removeUser|删除用户信息|请求入参：={}", id);
        return this.userService.removeUser(id);
    }

    /**
     * 修改用户信息
     *
     * @return
     * @exception
     * @date        2020/4/29 14:30
     */
    @ApiOperation(value = "修改用户信息", notes = "UserController user add")
    @PostMapping("/edit")
    public ResultVO<?> editUser(@RequestBody ResUserVO user)
    {
        log.info("UserController|editUser|修改用户信息|请求入参：={}", user);
        return this.userService.editUser(user);
    }

    /**
     * 查询用户信息
     *
     * @return
     * @exception
     * @date        2020/4/29 14:30
     */
    @ApiOperation(value = "查询用户信息", notes = "UserController user add")
    @GetMapping("/inquire")
    public ResultVO<?> inquireUser(@RequestParam Long id)
    {
        log.info("UserController|inquireUser|查询用户信息|请求入参：={}", id);
        return this.userService.inquireUser(id);
    }
}
