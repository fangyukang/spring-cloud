package org.qy.mybatisplus.client.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.qy.mybatisplus.client.domain.entity.User;

/**
 * @Description:	持久层
 * @Author:		方宇康
 * @CreateDate:	2020/4/29 15:35
 * @Version:      	1.0.0.1
 * @Company:       联通智网科技有限公司
 */
@Mapper
public interface UserDao extends BaseMapper<User>
{

}
