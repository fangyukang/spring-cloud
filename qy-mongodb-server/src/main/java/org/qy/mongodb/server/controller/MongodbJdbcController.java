package org.qy.mongodb.server.controller;

import lombok.extern.slf4j.Slf4j;
import org.qy.mongodb.server.service.MongodbJdbcService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Description:	数据库连接测试
 * @Author:		方宇康
 * @CreateDate:	2020/1/9 18:04
 * @Version:      	1.0.0.1
 * @Company:       联通智网科技有限公司
 */
@Slf4j
@RestController
@RequestMapping("/mongodb/")
public class MongodbJdbcController
{
    private final MongodbJdbcService mongodbJdbcService;

    @Autowired
    public MongodbJdbcController(MongodbJdbcService mongodbJdbcService)
    {
        this.mongodbJdbcService = mongodbJdbcService;
    }

    /**
     * 使用代码连接MongoDB
     *
     * @param
     * @return
     * @exception
     * @date        2020/1/9 18:12
     */
    @GetMapping("notAutowired")
    public void mongodbNotAutowired()
    {
        log.info("MongodbJdbcController|mongodbNoAutowired|使用代码连接MongoDB");
        this.mongodbJdbcService.mongodbNoAutowired();
    }

    /**
     * 使用注解连接MongoDB
     *
     * @param
     * @return
     * @exception
     * @date        2020/1/9 18:12
     */
    @GetMapping("autowired")
    public void mongodbAutowired()
    {
        log.info("MongodbJdbcController|mongodbAutowired|使用注解连接MongoDB");
        this.mongodbJdbcService.mongodbAutowired();
    }
}
