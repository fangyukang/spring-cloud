package org.qy.mongodb.server;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;

/**
 * @Description:	Mongodb 服务启动类
 * @Author:		方宇康
 * @CreateDate:	2020/1/9 17:21
 * @Version:      	1.0.0.1
 * @Company:       联通智网科技有限公司
 */
@Slf4j
@SpringBootApplication(scanBasePackages = {"org.qy.mongodb.server"})
public class MongodbApplication
{
    public static void main( String[] args )
    {
        ApplicationContext context = SpringApplication.run(MongodbApplication.class, args);
        String serverPort = context.getEnvironment().getProperty("server.port");
        log.info("Mongodb后台管理服务启动成功! Swagger2: http://127.0.0.1:".concat(serverPort).concat("/swagger-ui.html"));
    }

    @Bean
    public static PropertySourcesPlaceholderConfigurer placeholderConfigurer()
    {
        PropertySourcesPlaceholderConfigurer placeholder = new PropertySourcesPlaceholderConfigurer();
        placeholder.setIgnoreUnresolvablePlaceholders(true);
        return placeholder;
    }
}
