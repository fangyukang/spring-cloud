package org.qy.swagger.client.domain.entity;

import lombok.Data;

import java.io.Serializable;

/**
 * @Description:	des
 * @Author:		方宇康
 * @CreateDate:	2020/4/29 14:36
 * @Version:      	1.0.0.1
 * @Company:       联通智网科技有限公司
 */
@Data
public class User implements Serializable
{
    private static final long serialVersionUID = 1979352523248954320L;

    private String name;

    private Integer age;

    private String address;
}
