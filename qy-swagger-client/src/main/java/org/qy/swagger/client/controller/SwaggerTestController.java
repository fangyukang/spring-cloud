package org.qy.swagger.client.controller;

import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.qy.swagger.client.domain.vo.ResultVO;
import org.qy.swagger.client.enums.ResultEnum;
import org.qy.swagger.client.service.SwaggerTestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Description:	des
 * @Author:		方宇康
 * @CreateDate:	2020/4/29 11:49
 * @Version:      	1.0.0.1
 * @Company:       联通智网科技有限公司
 */
@Slf4j
@RestController
@RequestMapping("swagger/test")
public class SwaggerTestController
{
    private final SwaggerTestService swaggerTestService;

    @Autowired
    public SwaggerTestController(SwaggerTestService swaggerTestService)
    {
        this.swaggerTestService = swaggerTestService;
    }

    /**
     * 数据模拟
     *
     * @return
     * @exception
     * @date        2020/4/29 14:30
     */
    @ApiOperation(value = "获取测试MOCK数据", notes = "SwaggerTestController user")
    @PostMapping("/user")
    public ResultVO<?> user()
    {
        log.info("SwaggerTestController|user|进入获取测试MOCK数据接口服务");
        return ResultVO.build(ResultEnum.SUCCESS.getCode(), ResultEnum.SUCCESS.getMessage(), this.swaggerTestService.user());
    }
}
