package org.qy.swagger.client;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.ApplicationContext;

/**
 * @Description:	客户端 swagger集成
 * @Author:		方宇康
 * @CreateDate:	2020/4/29 11:43
 * @Version:      	1.0.0.1
 * @Company:       联通智网科技有限公司
 */
@Slf4j
@EnableDiscoveryClient
@SpringBootApplication(scanBasePackages = {"org.qy.swagger.client"})
public class SwaggerApplication
{
    public static void main( String[] args )
    {
        ApplicationContext context = SpringApplication.run(SwaggerApplication.class, args);
        String serverPort = context.getEnvironment().getProperty("server.port");
        log.info("后台管理服务客户端启动成功! Swagger2: http://127.0.0.1:".concat(serverPort).concat("/swagger-ui.html"));
    }
}
