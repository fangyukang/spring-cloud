package org.qy.swagger.client.service;

import lombok.extern.slf4j.Slf4j;
import org.qy.swagger.client.domain.entity.User;
import org.springframework.stereotype.Service;

/**
 * @Description:	业务层封装
 * @Author:		方宇康
 * @CreateDate:	2020/4/29 14:33
 * @Version:      	1.0.0.1
 * @Company:       联通智网科技有限公司
 */
@Slf4j
@Service
public class SwaggerTestService
{
    /**
     * 获取用户信息
     *
     * @return
     * @exception
     * @date        2020/4/29 14:35
     */
    public User user()
    {
        User user = new User();
        user.setName("张三");
        user.setAge(18);
        user.setAddress("");
        return user;
    }
}
