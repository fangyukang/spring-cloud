package org.qy.swagger.client.enums;

import lombok.Getter;
import lombok.NoArgsConstructor;

/**
 * @Description:	公共部分枚举类
 * @Author:		方宇康
 * @CreateDate:	2019/1/7
 * @Version:      	1.0.0.1
 * @Company:       联通智网科技有限公司
 */
@Getter
@NoArgsConstructor
public enum ResultEnum
{
	/** 访问参数异常 */
	ERROR_POST_PARAM("400", "访问参数异常"),
	/** 访问方法异常 */
	ERROR_POST_METHOD("405", "访问方法异常"),
	/** 访问异常 */
	FAILED("500", "访问异常,请稍后重试"),
	/** 访问成功 */
	SUCCESS("200","success"),
	/** 成功 */
	SUCCESS_ONE("0","成功")
    ;

	private String code;
	private String message;

	ResultEnum(String code, String message)
	{
		this.code = code;
		this.message = message;
	}
}