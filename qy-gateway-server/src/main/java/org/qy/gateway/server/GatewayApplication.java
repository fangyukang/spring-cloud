package org.qy.gateway.server;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.cloud.client.SpringCloudApplication;
import org.springframework.context.ApplicationContext;

/**
 * @Description:	网关启动类
 * @Author:		方宇康
 * @CreateDate:	2020/5/7 16:21
 * @Version:      	1.0.0.1
 * @Company:       联通智网科技有限公司
 */
@Slf4j
@SpringCloudApplication
public class GatewayApplication
{
    public static void main( String[] args )
    {
        ApplicationContext context = SpringApplication.run(GatewayApplication.class, args);
        String serverPort = context.getEnvironment().getProperty("server.port");
        log.info("网关服务启动成功! Swagger2: http://127.0.0.1:".concat(serverPort).concat("/swagger-ui.html"));
    }
}
