package org.qy.hbase.server.api;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/***
 *
 * All rights Reserved, Designed By www.faw.com.cn
 *
 * @Title: Property.java
 * @Package com.faw.iov.hbase.starter.api
 * @Description: 用于数据库字段名和java类做映射
 * @since JDK 1.8
 * @author zrl
 * @Email zhaorl@cu-sc.com
 * @version: v1.0.0
 * @date: 2019/12/12  11:03
 * @Copyright: 2019 www.faw.com.cn Inc. All rights reserved. <br/>
 * 注意：本内容仅限于一汽集团内部传阅，禁止外泄以及用于其他的商业目<br/>
 */
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
public @interface Property {
    String name();
}
