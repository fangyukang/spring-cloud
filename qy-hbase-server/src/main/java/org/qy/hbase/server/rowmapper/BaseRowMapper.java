package org.qy.hbase.server.rowmapper;

import lombok.Data;
import org.apache.hadoop.hbase.client.Result;
import org.qy.hbase.server.api.RowMapper;
import org.qy.hbase.server.utils.RowMapperUtils;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;

/**
 * Hbase公共Mapper
 * @param <T>
 */
@Data
public abstract class BaseRowMapper<T> implements RowMapper<T> {

    /**
     * 返回实体类型
     */
    private Class cls;

    public BaseRowMapper() {
        final Type t = this.getClass().getGenericSuperclass();
        if (ParameterizedType.class.isAssignableFrom(t.getClass())) {
            try {
                final String typeName = ((ParameterizedType) t).getActualTypeArguments()[0].getTypeName();
                this.cls = Class.forName(typeName);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public T mapRow(Result result, int i)
        throws Exception
    {
        return (T) RowMapperUtils.setRowValue(cls, result);
    }
}
