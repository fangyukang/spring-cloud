package org.qy.hbase.server;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

/**
 * All rights Reserved, Designed By www.cu-sc.com
 *
 * @Title:  HBaseApplication.java.java
 * @Package org.qy.hbase.server
 * @Description: TODO
 * @Since: JDK 1.8
 * @Author: fangyukang
 * @Email: fangyk@cu-sc.com
 * @Version: v1.0.0
 * @Date: 2020/5/26 11:11
 * @Copyright: 2020 www.cu-sc.com All rights reserved. <br/>
 */
@Slf4j
@SpringBootApplication(scanBasePackages = {"org.qy.hbase.server"})
public class HBaseApplication
{
    public static void main( String[] args )
    {
        ApplicationContext context = SpringApplication.run(HBaseApplication.class, args);
        String serverPort = context.getEnvironment().getProperty("server.port");
        log.info("HBase后台管理服务启动成功! Swagger2: http://127.0.0.1:".concat(serverPort).concat("/swagger-ui.html"));
    }
}
