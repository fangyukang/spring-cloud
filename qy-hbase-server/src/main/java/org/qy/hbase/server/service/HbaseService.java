package org.qy.hbase.server.service;

import org.apache.hadoop.hbase.client.Scan;
import org.qy.hbase.server.api.RowMapper;

import java.util.HashMap;
import java.util.List;

public interface HbaseService
{
    /**
     * 条件查询对应表属性
     * @param scan
     * @param rowMapper
     * @param <T>
     * @return
     */
    <T> List<T> find(Scan scan, RowMapper<T> rowMapper);

    /**
     * 根据条件查询表属性
     * @param tableName
     * @param scan
     * @param rowMapper
     * @param <T>
     * @return
     */
    <T> List<T> find(String tableName, Scan scan, RowMapper<T> rowMapper);

    /**
     * 根据条件查询
     *
     * @param tableName
     * @param scan
     * @param paramMap
     * @param rowMapper
     * @return
     */
    <T> List<T> getListByParam(String tableName, Scan scan, RowMapper<T> rowMapper, HashMap<String, String> paramMap);

    /**
     * rowkey查询
     * @param rowKey
     * @param rowMapper
     * @param <T>
     * @return
     */
    <T> T get(String rowKey, RowMapper<T> rowMapper);

    /**
     * rowKey查询
     * @param tableName
     * @param rowKey
     * @param rowMapper
     * @param <T>
     * @return
     */
    <T> T get(String tableName, String rowKey, RowMapper<T> rowMapper);


    /**
     * 更新或插入数据
     * @param t
     * @param <T>
     * @return
     */
    <T> int upsert(T t);

    /**
     * 更新或插入数据
     * @param tableName
     * @param t
     * @param <T>
     * @return
     */
    <T> int upsert(String tableName, T t);


    /**
     * 批量更新或插入数据
     * @param tableName
     * @param dataList
     * @param <T>
     * @return
     */
    <T> int upsert(String tableName, List<T> dataList);


    /**
     * 批量更新或插入数据
     * @param dataList
     * @param <T>
     * @return
     */
    <T> int upsert(List<T> dataList);

    /**
     * 批量更新或插入数据
     * @param dataList
     * @param <T>
     * @return
     */
    <T> int upsert(List<T> dataList, int flushSize);

    /**
     * 批量更新或插入数据
     * @param tableName
     * @param dataList
     * @param <T>
     * @return
     */
    <T> int upsert(String tableName, List<T> dataList, int flushSize);

    /**
     * 创建表
     * @param tableName 表名
     * @param splits 分区
     */
    void createTable(String tableName, byte[][] splits) throws Exception;

    /**
     * 创建表
     * @param tableName
     */
    void createTable(String tableName) throws Exception ;

}
