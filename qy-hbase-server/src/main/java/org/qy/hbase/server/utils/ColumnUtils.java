package org.qy.hbase.server.utils;

import org.apache.commons.lang.StringUtils;
import org.apache.hadoop.hbase.util.Bytes;

/**
 * 字段、值转换处理
 * @author HeQixuan
 */
public class ColumnUtils {

    /**
     * 下划线
     */
    public static final char UNDERLINE='_';

    /**
     * 值转换
     * @param source
     * @param type
     * @return
     */
    public static Object getValue(byte[] source, Class<?> type){
        if(StringUtils.equals(type.getSimpleName(), "String")){
            return Bytes.toString(source);
        }else if(StringUtils.equals(type.getSimpleName(), "Bigdecimal")){
            return Bytes.toBigDecimal(source);
        }else if(StringUtils.equals(type.getSimpleName(), "Integer")){
            return Bytes.toInt(source);
        }else if(StringUtils.equals(type.getSimpleName(), "Double")){
            return Bytes.toDouble(source);
        }else if(StringUtils.equals(type.getSimpleName(), "Fload")){
            return Bytes.toFloat(source);
        }else if(StringUtils.equals(type.getSimpleName(), "Long")){
            return Bytes.toLong(source);
        }else if(StringUtils.equals(type.getSimpleName(), "Short")){
            return Bytes.toShort(source);
        }else if(StringUtils.equals(type.getSimpleName(), "Boolean")){
            return Bytes.toBoolean(source);
        }
        return null;
    }

    /**
     * 基本驼峰转下划线后转大写
     * @param param
     * @return String
     */
    public static String camelToUnderline(String param){
        if("_S0000".equals(param)){
            return param;
        }
        if (param==null||"".equals(param.trim())){
            return "";
        }
        int len=param.length();
        StringBuilder sb=new StringBuilder(len);
        for (int i = 0; i < len; i++) {
            char c=param.charAt(i);
            if (i != 0 && Character.isUpperCase(c)){
                sb.append(UNDERLINE).append(c);
            }else{
                sb.append(c);
            }
        }
        return sb.toString().toUpperCase();
    }

}
