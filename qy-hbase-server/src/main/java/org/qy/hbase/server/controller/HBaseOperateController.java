package org.qy.hbase.server.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * All rights Reserved, Designed By www.cu-sc.com
 *
 * @Title: HbaseOperateControlle.java
 * @Package org.qy.hbase.server.controller
 * @Description: TODO
 * @Since: JDK 1.8
 * @Author: fangyukang
 * @Email: fangyk@cu-sc.com
 * @Version: v1.0.0
 * @Date: 2020/5/26 11:33
 * @Copyright: 2020 www.cu-sc.com All rights reserved. <br/>
 */
@Slf4j
@RestController
@RequestMapping("/hbase/")
public class HBaseOperateController
{

}
