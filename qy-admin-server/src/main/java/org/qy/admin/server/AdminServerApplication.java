package org.qy.admin.server;

import de.codecentric.boot.admin.server.config.EnableAdminServer;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @Description:	des
 * @Author:		方宇康
 * @CreateDate:	2020/5/11 11:36
 * @Version:      	1.0.0.1
 * @Company:       联通智网科技有限公司
 */
@Slf4j
@EnableAdminServer
@SpringBootApplication
public class AdminServerApplication
{
    public static void main( String[] args )
    {
        SpringApplication.run(AdminServerApplication.class, args);
        log.info("Admin服务监控平台启动成功! http://127.0.0.1:8086");
    }
}
